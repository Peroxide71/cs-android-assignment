package com.backbase.assignment

import com.backbase.assignment.model.data.Constants
import com.backbase.assignment.model.data.MovieDetails
import com.backbase.assignment.model.data.PageResult
import com.backbase.assignment.model.datasource.retrofit.RetrofitHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import org.junit.Before
import org.junit.Test
import org.mockito.MockitoAnnotations


class RetrofitTest {

    private var retrofitHelper: RetrofitHelper? = RetrofitHelper

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun fetchValidVerticalDataShouldLoadIntoView() {
        val pageResult = PageResult(null, null, null, null)
        GlobalScope.async(Dispatchers.Main) {
            val result = retrofitHelper?.MOVIES_API?.fetchPopularAsync("en-US", 1, Constants.API_KEY_VALUE)?.await()
            assert(result?.body() == pageResult)
        }
    }

    @Test
    fun fetchValidHorizontalDataLoadedIntoView() {
        val pageResult = PageResult(null, null, null, null)
        GlobalScope.async(Dispatchers.Main) {
            val result = retrofitHelper?.MOVIES_API?.fetchNowPlayingAsync("en-US", 1, Constants.API_KEY_VALUE)?.await()
            assert(result?.body() == pageResult)
        }
    }

    @Test
    fun fetchValidDetailDataLoadedIntoView() {
        val pageResult = MovieDetails(null,
                null, null, null, null,
                null, null, null, null,
        null, null, null, null, null,
        null, null, null, null, null,
        null, null, null, null, null,null)
        GlobalScope.async(Dispatchers.Main) {
            val result = retrofitHelper?.MOVIES_API?.fetchMovieByIdAsync(0, Constants.API_KEY_VALUE)?.await()
            assert(result == pageResult)
        }
    }
}