package com.backbase.assignment.viewmodel

data class PlayingNowMovieViewModel(
        var id: Int,
        var posterLink: String?,
        var onClick: (id: Int) -> Unit) {
    fun onClickEvent(id: Int) {
        onClick.invoke(id)
    }
}