package com.backbase.assignment.viewmodel.databinding

import com.backbase.assignment.viewmodel.PlayingNowMovieViewModel
import com.backbase.assignment.viewmodel.databinding.base.BindableDataWrapperBase

class BindableDataWrapperImplHorizontal (
        var data: MutableList<PlayingNowMovieViewModel>,
        override var onClick: (id: Int) -> Unit,
        override var onScrollBottomReach: () -> Unit): BindableDataWrapperBase(onClick, onScrollBottomReach)