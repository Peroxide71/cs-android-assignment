package com.backbase.assignment.viewmodel.databinding.base

abstract class BindableDataWrapperBase(open var onClick: (id: Int) -> Unit,
                                       open var onScrollBottomReach: () -> Unit)