package com.backbase.assignment.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.backbase.assignment.model.data.MovieDetails
import com.backbase.assignment.model.usecase.MoviesUseCase
import com.backbase.assignment.viewmodel.databinding.BindableDataWrapperImplHorizontal
import com.backbase.assignment.viewmodel.databinding.BindableDataWrapperImplVertical
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

open class MainActivityViewModel: ViewModel(){
    val bindableHoizontalObservableField: ObservableField<BindableDataWrapperImplHorizontal>
            = ObservableField(BindableDataWrapperImplHorizontal(mutableListOf(), ::fetchMovieById, ::fetchNowPlayingMovies))
    val bindableVerticalObservableField: ObservableField<BindableDataWrapperImplVertical>
            = ObservableField(BindableDataWrapperImplVertical(mutableListOf(), ::fetchMovieById, ::fetchMostPopularMovies))
    val screenLiveData: MutableLiveData<MovieDetails?> by lazy {
        MutableLiveData<MovieDetails?>()
    }
    private var pageSize: Int = 20
    private var horizontalPageSize: Int = 20

    fun fetchMostPopularMovies() {
        viewModelScope.launch (Dispatchers.Main){
            val previousDatasetSize = bindableVerticalObservableField.get()?.data?.size ?: 0
            val nextPageNumber = (previousDatasetSize / pageSize) + 1
            val result = MoviesUseCase.fetchMostPopularMoviesPageAsync(nextPageNumber)?.await()
            val newList: MutableList<VerticalMovieItemViewModel>? = bindableVerticalObservableField.get()?.data
            val vmResults = result?.results?.map { VerticalMovieItemViewModel(
                    it.id,
                    it.title,
                    it.poster_path,
                    it.release_date,
                    it.popularity.toInt()
            ) {}
            }?.toMutableList()
            newList?.addAll(vmResults as Iterable<VerticalMovieItemViewModel>)
            bindableVerticalObservableField.get()?.data = newList ?: mutableListOf()
            bindableVerticalObservableField.notifyChange()
            pageSize = result?.results?.size ?: 0
        }
    }

    fun fetchNowPlayingMovies() {
        viewModelScope.launch (Dispatchers.Main){
            val previousDatasetSize = bindableHoizontalObservableField.get()?.data?.size ?: 0
            val nextPageNumber = (previousDatasetSize / horizontalPageSize) + 1
            val result = MoviesUseCase.fetchNowPlayingMoviesPageAsync(nextPageNumber)?.await()
            val newList: MutableList<PlayingNowMovieViewModel>? = bindableHoizontalObservableField.get()?.data
            val vmResults = result?.results?.map { PlayingNowMovieViewModel(
                    it.id,
                    it.poster_path
            ) {}
            }?.toMutableList()
            newList?.addAll(vmResults as Iterable<PlayingNowMovieViewModel>)
            bindableHoizontalObservableField.get()?.data = newList ?: mutableListOf()
            bindableHoizontalObservableField.notifyChange()
            horizontalPageSize = result?.results?.size ?: 0
        }
    }

    private fun fetchMovieById(id: Int) {
        viewModelScope.launch {
            val result = MoviesUseCase.fetchMovieByIdAsync(id)?.await()
            screenLiveData.value = result
        }
    }

    override fun onCleared() {
        MoviesUseCase.cancelAllRequests()
        super.onCleared()
    }
}
