package com.backbase.assignment.viewmodel

import androidx.lifecycle.ViewModel
import com.backbase.assignment.model.data.MovieDetails

class MovieDetailsViewModel : ViewModel() {
    var movieDetails: MovieDetails? = null
}