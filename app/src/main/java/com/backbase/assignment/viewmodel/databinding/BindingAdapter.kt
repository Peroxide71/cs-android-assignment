package com.backbase.assignment.viewmodel.databinding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.backbase.assignment.model.data.Genres
import com.backbase.assignment.view.adapter.GenresAdapter
import com.backbase.assignment.view.adapter.PlayingNowMoviesAdapter
import com.backbase.assignment.view.adapter.PopularMoviesAdapter
import com.backbase.assignment.view.adapter.base.AdapterInteractor
import com.backbase.assignment.view.custom.RatingView
import com.backbase.assignment.viewmodel.databinding.base.BindableDataWrapperBase
import com.bumptech.glide.Glide

object BindingAdapter {

    //    Adapter methods to bind specific values to views.
    @JvmStatic
    @BindingAdapter("bind:srcUrl")
    fun setImageUrl(view: ImageView, url: String?) {
        url?.let {
            Glide.with(view.context).load("https://image.tmdb.org/t/p/original/${it}").into(view)
        }
    }

    @JvmStatic
    fun onItemClicked(recyclerView: RecyclerView, bindableDataWrapper: BindableDataWrapperBase) {
        (recyclerView.adapter as? AdapterInteractor)?.executeOnClick(bindableDataWrapper.onClick)
    }

    @JvmStatic
    @BindingAdapter("bind:verticalData")
    fun setData(recyclerView: RecyclerView, bindableDataWrapper: BindableDataWrapperImplVertical) {
        (recyclerView.adapter as? PopularMoviesAdapter)?.updateList(bindableDataWrapper.data) ?: run {
            val adapter = PopularMoviesAdapter()
            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
            recyclerView.adapter = adapter
        }
        setRecyclerViewScrollCallback(recyclerView, bindableDataWrapper)
        onItemClicked(recyclerView, bindableDataWrapper)
    }

    @JvmStatic
    @BindingAdapter("bind:horizontalData")
    fun setHorizontalData(recyclerView: RecyclerView, bindableDataWrapper: BindableDataWrapperImplHorizontal) {
        (recyclerView.adapter as? PlayingNowMoviesAdapter)?.let {
            it.updateList(bindableDataWrapper.data)
        } ?: run {
            val adapter = PlayingNowMoviesAdapter()
            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context, LinearLayoutManager.HORIZONTAL, false)
            recyclerView.adapter = adapter
        }
        setRecyclerViewScrollCallback(recyclerView, bindableDataWrapper)
        onItemClicked(recyclerView, bindableDataWrapper)
    }

    @JvmStatic
    @BindingAdapter("bind:rating")
    fun bindRating(ratingView: RatingView, rating: Float) {
        ratingView.rating = rating
    }

    @JvmStatic
    @BindingAdapter("bind:genres")
    fun bindGenres(recyclerView: RecyclerView, genres: MutableList<Genres>?) {
        (recyclerView.adapter as? GenresAdapter)?.let{
            it.data = genres ?: mutableListOf()
            it.notifyDataSetChanged()
        } ?: run {
            val adapter = GenresAdapter()
            adapter.data = genres ?: mutableListOf()
            adapter.notifyDataSetChanged()
            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context, LinearLayoutManager.VERTICAL, false)
            recyclerView.adapter = adapter
        }
    }

    @JvmStatic
    fun setRecyclerViewScrollCallback(recyclerView: RecyclerView, bindableDataWrapper: BindableDataWrapperBase) {
        (recyclerView.adapter as? AdapterInteractor)?.let {
            recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    val directionCriteria: Boolean = if (it is PopularMoviesAdapter) {
                        !recyclerView.canScrollVertically(1)
                    } else {
                        !recyclerView.canScrollHorizontally(1)
                    }
                    if (directionCriteria && it.currentState == AdapterInteractor.AdapterState.STATE_DONE) {
                        it.currentState = AdapterInteractor.AdapterState.STATE_LOADING
                        bindableDataWrapper.onScrollBottomReach.invoke()
                    }
                }
            })
        }
    }
}
