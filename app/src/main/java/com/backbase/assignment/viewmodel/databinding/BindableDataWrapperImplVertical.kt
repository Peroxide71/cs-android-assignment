package com.backbase.assignment.viewmodel.databinding

import com.backbase.assignment.viewmodel.VerticalMovieItemViewModel
import com.backbase.assignment.viewmodel.databinding.base.BindableDataWrapperBase

class BindableDataWrapperImplVertical(
        var data: MutableList<VerticalMovieItemViewModel>,
        override var onClick: (id: Int) -> Unit,
        override var onScrollBottomReach: () -> Unit): BindableDataWrapperBase(onClick, onScrollBottomReach) {
}