package com.backbase.assignment.viewmodel

class VerticalMovieItemViewModel(
        var id: Int,
        var title: String,
        var posterLink: String?,
        var releaseDate: String?,
        var rating: Int?,
        var onClick: (id: Int) -> Unit) {
    fun onClickEvent(id: Int) {
        onClick.invoke(id)
    }
}