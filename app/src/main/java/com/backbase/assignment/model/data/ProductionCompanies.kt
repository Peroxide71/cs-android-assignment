package com.backbase.assignment.model.data
data class ProductionCompanies(val logo_path: String,
                          val name: String,
                          val id: Int,
                          val origin_country: String)
