package com.backbase.assignment.model.datasource.retrofit

import com.backbase.assignment.model.data.Constants
import com.backbase.assignment.model.data.MovieDetails
import com.backbase.assignment.model.data.PageResult
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

object RetrofitHelper {
    private val loggingInterceptor = HttpLoggingInterceptor()

    private val retrofit = Retrofit.Builder()
            .baseUrl(Constants.REST_ENDPOINT)
            .client(
                    OkHttpClient.Builder().addInterceptor(
                            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
                    ).build()
            )
            //Adapters and converter factories required to put Retrofit calls to coroutines.
            .addConverterFactory(GsonConverterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
    val MOVIES_API: MoviesApi? = retrofit.create(MoviesApi::class.java)

    interface MoviesApi {

        @GET(Constants.GET_NOW_PLAYING)
        fun fetchNowPlayingAsync(@Query(Constants.LANGUAGE)language : String,
                                 @Query(Constants.PAGE)page: Int,
                                 @Query(Constants.API_KEY)apiKey: String): Deferred<Response<PageResult>>

        @GET(Constants.GET_POPULAR)
        fun fetchPopularAsync(@Query(Constants.LANGUAGE)language : String,
                              @Query(Constants.PAGE)page: Int,
                              @Query(Constants.API_KEY)apiKey: String): Deferred<Response<PageResult>>

        @GET(Constants.GET_MOVIE_BY_ID)
        fun fetchMovieByIdAsync(@Path("MOVIE_ID") id: Int,
                                @Query(Constants.API_KEY)apiKey: String): Deferred<MovieDetails>
    }
}