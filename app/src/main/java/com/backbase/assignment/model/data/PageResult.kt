package com.backbase.assignment.model.data

data class PageResult(val page: Int?,
                      val total_results: Int?,
                      val total_pages: Int?,
                      val results: List<Movie>?)