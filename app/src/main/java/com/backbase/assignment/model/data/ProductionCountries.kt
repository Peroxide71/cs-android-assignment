package com.backbase.assignment.model.data
data class ProductionCountries(val iso_3166_1: String, val name: String)
