package com.backbase.assignment.model.data

object Constants {
    const val REST_ENDPOINT = "https://api.themoviedb.org/3/"
    const val API_KEY_VALUE = "55957fcf3ba81b137f8fc01ac5a31fb5"
    const val GET_NOW_PLAYING = "movie/now_playing"
    const val GET_POPULAR = "movie/popular"
    const val GET_MOVIE_BY_ID = "movie/ {MOVIE_ID}"
    const val LANGUAGE = "language"
    const val PAGE = "page"
    const val API_KEY = "api_key"
}