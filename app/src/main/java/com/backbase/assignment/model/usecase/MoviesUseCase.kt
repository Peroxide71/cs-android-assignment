package com.backbase.assignment.model.usecase

import com.backbase.assignment.model.data.Constants
import com.backbase.assignment.model.data.MovieDetails
import com.backbase.assignment.model.data.PageResult
import com.backbase.assignment.model.datasource.retrofit.RetrofitHelper
import kotlinx.coroutines.*

object MoviesUseCase {
    private var coroutineScope: CoroutineScope? = CoroutineScope(Dispatchers.IO)

    fun fetchNowPlayingMoviesPageAsync(page: Int): Deferred<PageResult?>? {
        return coroutineScope?.async {
            val response = RetrofitHelper.MOVIES_API?.fetchNowPlayingAsync("en-US", page, Constants.API_KEY_VALUE)?.await()
            response?.body()
        }
    }

    fun fetchMostPopularMoviesPageAsync(page: Int) : Deferred<PageResult?>? {
        return coroutineScope?.async {
            val response = RetrofitHelper.MOVIES_API?.fetchPopularAsync("en-US", page, Constants.API_KEY_VALUE)?.await()
            response?.body()
        }
    }

    fun fetchMovieByIdAsync(id: Int): Deferred<MovieDetails?>?  {
        return coroutineScope?.async {
            val response = RetrofitHelper.MOVIES_API?.fetchMovieByIdAsync(id, Constants.API_KEY_VALUE)?.await()
            response
        }
    }

    fun cancelAllRequests() {
        coroutineScope?.cancel()
    }
}