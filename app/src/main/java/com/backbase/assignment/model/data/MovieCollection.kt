package com.backbase.assignment.model.data


data class MovieCollection(val backdrop_path: String,
                           val name: String,
                           val id: Int,
                           val poster_path: String)