package com.backbase.assignment.view.adapter.utils

import androidx.recyclerview.widget.DiffUtil
import com.backbase.assignment.viewmodel.PlayingNowMovieViewModel

class NowPlayingMoviesDiffCallback(var oldData: MutableList<PlayingNowMovieViewModel>,
                                   var newData: MutableList<PlayingNowMovieViewModel>) : DiffUtil.Callback() {


    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldData[oldItemPosition].id == newData[newItemPosition].id

    override fun getOldListSize(): Int = oldData.size

    override fun getNewListSize(): Int = newData.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldData[oldItemPosition] == newData[newItemPosition]
}