package com.backbase.assignment.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.backbase.assignment.R
import com.backbase.assignment.databinding.GenreItemBinding
import com.backbase.assignment.model.data.Genres

class GenresAdapter : RecyclerView.Adapter<GenresAdapter.ViewHolder>(){
    var data: MutableList<Genres> = mutableListOf()
    var layoutInflater: LayoutInflater? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if(layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        val binding: GenreItemBinding = DataBindingUtil.inflate(
                layoutInflater!!,
                R.layout.genre_item,
                parent,
                false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val viewModel = data[position]
        holder.itemBinding.item = viewModel
        holder.itemBinding.executePendingBindings()
    }

    class ViewHolder(binding: GenreItemBinding): RecyclerView.ViewHolder(binding.root){
        var itemBinding = binding
    }
}