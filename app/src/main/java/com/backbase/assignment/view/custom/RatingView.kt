package com.backbase.assignment.view.custom

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import com.backbase.assignment.view.util.ColorUtils


class RatingView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr){
    var rating: Float = 0f
    private var mSize = 0
    private var mPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var backGroundPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var mRect: RectF = RectF(5f, 5f, 77f, 77f)

    init {
        backGroundPaint.style = Paint.Style.STROKE
        backGroundPaint.strokeWidth = 8f
        backGroundPaint.strokeCap = Paint.Cap.BUTT
        mPaint.style = Paint.Style.STROKE
        mPaint.strokeWidth = 8f
        mPaint.strokeCap = Paint.Cap.BUTT
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        val xPad = paddingLeft + paddingRight
        val yPad = paddingTop + paddingBottom
        val width = measuredWidth - xPad
        val height = measuredHeight - yPad
        mSize = if (width < height) width else height
        setMeasuredDimension(mSize + xPad, mSize + yPad)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        mPaint.color = ColorUtils.getColor(rating)
        canvas?.drawArc(mRect, 270f, rating, false, mPaint)
    }
}