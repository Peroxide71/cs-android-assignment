package com.backbase.assignment.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.backbase.assignment.R
import com.backbase.assignment.databinding.MovieNowPlayingItemBinding
import com.backbase.assignment.view.adapter.base.AdapterInteractor
import com.backbase.assignment.view.adapter.utils.NowPlayingMoviesDiffCallback
import com.backbase.assignment.viewmodel.PlayingNowMovieViewModel


class PlayingNowMoviesAdapter :
        RecyclerView.Adapter<PlayingNowMoviesAdapter.ViewHolder>(), AdapterInteractor {
    var listData: MutableList<PlayingNowMovieViewModel> = mutableListOf()
    var layoutInflater: LayoutInflater? = null
    override var currentState: AdapterInteractor.AdapterState = AdapterInteractor.AdapterState.STATE_DONE

    private var onClick: (id: Int) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if(layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        val binding: MovieNowPlayingItemBinding = DataBindingUtil.inflate(layoutInflater!!,
                R.layout.movie_now_playing_item,
                parent,
                false)
        return ViewHolder(binding)
    }

    fun updateList(newList: MutableList<PlayingNowMovieViewModel>) {
        currentState = AdapterInteractor.AdapterState.STATE_DONE
        val diffResult = DiffUtil.calculateDiff(NowPlayingMoviesDiffCallback(this.listData, newList))
        diffResult.dispatchUpdatesTo(this)
        this.listData.addAll(newList.distinct())
    }

    override fun getItemCount(): Int = listData.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val viewModel = listData[position]
        viewModel.onClick = onClick
        holder.itemBinding.item = viewModel
        holder.itemBinding.executePendingBindings()
    }

    override fun executeOnClick(onClick: (id: Int) -> Unit) {
        this.onClick = onClick
    }

    class ViewHolder(binding: MovieNowPlayingItemBinding) : RecyclerView.ViewHolder(binding.root) {
        var itemBinding = binding
    }
}
