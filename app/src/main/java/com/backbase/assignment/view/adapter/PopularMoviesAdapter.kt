package com.backbase.assignment.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.backbase.assignment.R
import com.backbase.assignment.databinding.MovieItemBinding
import com.backbase.assignment.view.adapter.base.AdapterInteractor
import com.backbase.assignment.view.adapter.utils.PopularMoviesDiffCallback
import com.backbase.assignment.viewmodel.VerticalMovieItemViewModel

class PopularMoviesAdapter :
    RecyclerView.Adapter<PopularMoviesAdapter.ViewHolder>(), AdapterInteractor {
    var listData: MutableList<VerticalMovieItemViewModel> = mutableListOf()
    var layoutInflater: LayoutInflater? = null
    private var onClick: (id: Int) -> Unit = {}
    override var currentState: AdapterInteractor.AdapterState = AdapterInteractor.AdapterState.STATE_DONE
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if(layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        val binding: MovieItemBinding = DataBindingUtil.inflate(layoutInflater!!,
                R.layout.movie_item,
                parent,
                false)
        return ViewHolder(binding)
    }

    fun updateList(newList: MutableList<VerticalMovieItemViewModel>) {
        currentState = AdapterInteractor.AdapterState.STATE_DONE
        val diffResult = DiffUtil.calculateDiff(PopularMoviesDiffCallback(this.listData, newList))
        diffResult.dispatchUpdatesTo(this)
        this.listData.addAll(newList.distinct())
    }

    override fun executeOnClick(onClick: (id: Int) -> Unit) {
        this.onClick = onClick
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val viewModel = listData[position]
        viewModel.onClick = onClick
        holder.itemBinding.item = viewModel
        holder.itemBinding.executePendingBindings()
    }

    override fun getItemCount() = listData.size

    class ViewHolder(binding: MovieItemBinding) : RecyclerView.ViewHolder(binding.root) {
        var itemBinding: MovieItemBinding = binding
    }
}
