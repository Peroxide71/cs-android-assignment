package com.backbase.assignment.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.backbase.assignment.BR
import com.backbase.assignment.R
import com.backbase.assignment.databinding.MovieDetailsFragmentBinding
import com.backbase.assignment.viewmodel.MainActivityViewModel
import com.backbase.assignment.viewmodel.MovieDetailsViewModel

class MovieDetailsFragment : Fragment() {

    companion object {
        fun newInstance() = MovieDetailsFragment()
    }

    private val viewModel: MovieDetailsViewModel by viewModels()
    private val sharedViewModel: MainActivityViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: MovieDetailsFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.movie_details_fragment, container, false)
        binding.setVariable(BR.viewModel, viewModel)
        viewModel.movieDetails = sharedViewModel.screenLiveData.value
        return binding.root
    }

}