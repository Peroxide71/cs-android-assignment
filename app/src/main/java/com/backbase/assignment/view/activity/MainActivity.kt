package com.backbase.assignment.view.activity

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.backbase.assignment.BR
import com.backbase.assignment.R
import com.backbase.assignment.databinding.ActivityMainBinding
import com.backbase.assignment.view.fragment.MovieDetailsFragment
import com.backbase.assignment.viewmodel.MainActivityViewModel


class MainActivity : AppCompatActivity() {

    private val viewModel: MainActivityViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.setVariable(BR.viewModel, viewModel)
        viewModel.screenLiveData.observe(this, Observer {
            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.container, MovieDetailsFragment.newInstance())
                    .addToBackStack(null)
                    .commit()
        })
        viewModel.fetchNowPlayingMovies()
        viewModel.fetchMostPopularMovies()
    }
}
