package com.backbase.assignment.view.util

import android.graphics.Color
import androidx.annotation.ColorInt

object ColorUtils {
    private const val FIRST_COLOR = Color.GREEN
    private const val SECOND_COLOR = Color.YELLOW
    private const val THIRD_COLOR = Color.RED

    fun getColor(p: Float): Int {
        when(p.toInt()) {
            in 0..100 -> return THIRD_COLOR
            in 100..200 -> return SECOND_COLOR
            in 200..360 -> return FIRST_COLOR
            else -> return THIRD_COLOR
        }
    }

    @ColorInt
    private fun adjustAlpha(@ColorInt color: Int, factor: Float): Int {
        val alpha = Math.round(Color.alpha(color) * factor)
        val red = Color.red(color)
        val green = Color.green(color)
        val blue = Color.blue(color)
        return Color.argb(alpha, red, green, blue)
    }
}