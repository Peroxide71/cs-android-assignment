package com.backbase.assignment.view.adapter.utils

import androidx.recyclerview.widget.DiffUtil
import com.backbase.assignment.viewmodel.VerticalMovieItemViewModel

class PopularMoviesDiffCallback(var oldData: MutableList<VerticalMovieItemViewModel>,
                                var newData: MutableList<VerticalMovieItemViewModel>) : DiffUtil.Callback() {


    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldData[oldItemPosition].id == newData[newItemPosition].id

    override fun getOldListSize(): Int = oldData.size

    override fun getNewListSize(): Int = newData.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldData[oldItemPosition] == newData[newItemPosition]
}