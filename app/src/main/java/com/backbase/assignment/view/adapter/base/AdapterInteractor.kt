package com.backbase.assignment.view.adapter.base

interface AdapterInteractor {
    var currentState: AdapterState
    fun executeOnClick(onClick: (id: Int) -> Unit)

    enum class AdapterState{
        STATE_LOADING,
        STATE_DONE
    }
}