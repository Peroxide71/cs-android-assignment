# Android Assignment CS Solution

I have implemented the application as requested.

 - Architecture:
MVVM pattern is used.
All views manipulations inside the screen done exclusively by databinding. No explicit view manipulations are present in Activity or Fragment, which makes their code much cleaner and less view-related issues are possible.
All complex bindings are executed through dedicated BindingAdapter class.
List adapters also use databinding which makes their code much shorter and easier to read.
Package structure is organised strictly accordingly to pattern, so each source file is defined either as a part of Model, View, or ViewModel layers.
Model layer is also splitted into UseCase, Data and Datasource layers. 

- Libraies used:
Retrofit for network interaction.
Coroutines are used for asynchronous operations during Retrofit network operations.
Android JetPack ViewModel and LiveData used for MVVM pattern implementation.
Glide is used for image processing and caching. All the image magic happens under the hood of this beautiful library once view and image endpoint are provided.
Junit and Mockito are used for unit testing and object mocking.

- Known issues
1. Rating view is not implemented exactly by design. For some reason setting Paint object in customized view in Android as semi-transparent doesn't let to make a part of the view semi-transparent. As well, manipulating alpha-channel of the color does not give the desired result. This problems requires some further investigation which is out of the scope for this assignment.
So only non-transparent part of the custom view was implemented, which still makes it look decent.
2. Mockito was not able to mock the ViewModel functions calls, because they utilise some of the latest android.arch.lifecycle:extensions
artifacts like viewModelScope which is not trivial to mock and also reuires further investigation.
